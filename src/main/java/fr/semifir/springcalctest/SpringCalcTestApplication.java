package fr.semifir.springcalctest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCalcTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCalcTestApplication.class, args);
    }

}
